#include <Wire.h>
#include "Adafruit_MCP23017.h"

#define PIN_STATUS_LED 16

#define MCP_PIN_BUZZER 5 // buzzer on mcp
#define MCP_PIN_LATCH 13

#define MCP_INPUT_ESTOP 3
#define MCP_INPUT_UP 0
#define MCP_INPUT_DOWN 1
#define MCP_INPUT_ALARM 2

#define MCP_INPUT_GATE 8
uint8_t gateIsClosed = false;
#define MCP_INPUT_LIFT_MIN 9
#define MCP_INPUT_LIFT_MAX 10
#define MCP_INPUT_RAMP_MIN 11
#define MCP_INPUT_RAMP_MAX 12

#define MOTOR1_INH_MCP 7
#define MOTOR1_IN1 9
#define MOTOR1_IN2 10

#define MOTOR2_INH_MCP 6
#define MOTOR2_IN1 5
#define MOTOR2_IN2 6

int motor1_speed = 0; //-255 full reverse, 0 stop, 255 full forward
int motor1_target = 0;
int motor2_speed = 0; //-255 full reverse, 0 stop, 255 full forward
int motor2_target = 0;

Adafruit_MCP23017 mcp;

long subtaskStartTime = 0;
int subtaskTimeout = 30000; // 30 second timeout for every action

#define FLASH_IDLE 1
#define FLASH_MOVING_UP 2
#define FLASH_MOVING_DOWN 3
#define FLASH_ESTOP_PRESSED 5
#define FLASH_OVERRIDE_MODE 7

int flashCount = 1;
int currentFlashCount = 0;
long lastFlash = 0;
int longFlashInterval = 3000;
int shortFlashInterval = 300;

#define MODE_NORMAL 0        // Normal mode, ready for button presses and standard tasks
#define MODE_TEST 1          // In test mode, buttons don't trigger tasks
#define MODE_ESTOP_PRESSED 2 // Estop pressed, hold estop for override, any other button switches back to normal
#define MODE_OVERRIDE 3      // Tasks disabled, gate unlocked, up/down directly controls lift motors

int currentMode = MODE_NORMAL;

// If estop held longer than interval, go to override mode (direct up/down control, tasks disabled)
bool estopDown = false;
long eStopDownTime = 0;
int estopOverrideInterval = 10000;

int temp = 0;

void setup()
{
  InitComms();
  LoadSettings();
  mcp.begin(); // use default address 0

  pinMode(PIN_STATUS_LED, OUTPUT);

  pinMode(MOTOR1_IN1, OUTPUT);
  pinMode(MOTOR1_IN2, OUTPUT);
  mcp.pinMode(MOTOR1_INH_MCP, OUTPUT);
  mcp.digitalWrite(MOTOR1_INH_MCP, LOW);

  pinMode(MOTOR2_IN1, OUTPUT);
  pinMode(MOTOR2_IN2, OUTPUT);
  mcp.pinMode(MOTOR2_INH_MCP, OUTPUT);
  mcp.digitalWrite(MOTOR2_INH_MCP, LOW);

  mcp.pinMode(MCP_PIN_BUZZER, OUTPUT);
  mcp.pinMode(MCP_PIN_LATCH, OUTPUT);
  SetLatchOpen(true);

  mcp.pinMode(MCP_INPUT_ESTOP, INPUT);
  mcp.pinMode(MCP_INPUT_UP, INPUT);
  mcp.pinMode(MCP_INPUT_DOWN, INPUT);
  mcp.pinMode(MCP_INPUT_ALARM, INPUT);
  mcp.pinMode(MCP_INPUT_GATE, INPUT);
  mcp.pinMode(MCP_INPUT_LIFT_MIN, INPUT);
  mcp.pinMode(MCP_INPUT_LIFT_MAX, INPUT);
  mcp.pinMode(MCP_INPUT_RAMP_MIN, INPUT);
  mcp.pinMode(MCP_INPUT_RAMP_MAX, INPUT);

  // SetCurrentMode(MODE_OVERRIDE);
}

void loop()
{
  CheckComms();

  FlashIndicator();

  if (currentMode == MODE_NORMAL || currentMode == MODE_ESTOP_PRESSED)
  {
    CheckInputs();
    CheckEStop();
    UpdateMotorControl();

    DoTasks();
  }
  else if (currentMode == MODE_OVERRIDE)
  {
    CheckEStop();
    // Serial.println("override");
    if (ButtonStateEstop())
    {
      if (ButtonStateINPUT_UP())
      {
        SetMotorSpeed(1, 255);
        eStopDownTime = millis();
      }
      else if (ButtonStateINPUT_DOWN())
      {
        SetMotorSpeed(1, -255);
        eStopDownTime = millis();
      }
      else
      {
        SetMotorSpeed(1, 0);
      }
    }
    else
    {
      if (ButtonStateINPUT_UP())
      {
        SetMotorSpeed(2, 255);
      }
      else if (ButtonStateINPUT_DOWN())
      {
        SetMotorSpeed(2, -255);
      }
      else
      {
        SetMotorSpeed(2, 0);
      }
    }
  }

  delay(10);
}

void SetCurrentMode(int newMode)
{
  currentMode = newMode;

  // Set indicator flash count
  switch (currentMode)
  {
  case MODE_NORMAL:
    SetIndicatorFlashCount(FLASH_IDLE);
    break;
  case MODE_ESTOP_PRESSED:
    SetIndicatorFlashCount(FLASH_ESTOP_PRESSED);
    break;
  case MODE_OVERRIDE:
    SetIndicatorFlashCount(FLASH_OVERRIDE_MODE);
    break;
  }
  Serial.print("Mode: ");
  Serial.println(currentMode);
}

void CheckInputs()
{
  // if up or down button pushed and not already doing so or in that position, begin the action
  if (ButtonStateINPUT_UP())
  {
    if (!IsGoingUp() && !ButtonStateLIFT_MAX())
    {
      StartActionGoUp();
    }
  }
  if (ButtonStateINPUT_DOWN())
  {
    if (!IsGoingDown() && !ButtonStateLIFT_MIN())
    {
      StartActionGoDown();
    }
  }

  gateIsClosed = ButtonStateGATE();
  if (!gateIsClosed)
  {
    if (IsGoingUp() || IsGoingDown())
    {
      EmergencyStop();
    }
  }

  if (ButtonStateINPUT_ALARM())
  {
    SetBuzzerActive(true);
  }
  else
  {
    SetBuzzerActive(false);
  }
}

void CheckEStop()
{
  if (ButtonStateEstop())
  {
    // if (currentMode == MODE_NORMAL)
    // {
    //   EmergencyStop();
    // }

    if (!estopDown)
    {
      Serial.println("Estop Down");
      if (currentMode != MODE_OVERRIDE)
      {
        EmergencyStop();
      }
      estopDown = true;
      eStopDownTime = millis();
    }
    else if (millis() - eStopDownTime > estopOverrideInterval) // Switch to Override mode
    {
      if (currentMode == MODE_ESTOP_PRESSED)
      {
        SetCurrentMode(MODE_OVERRIDE);
        for (int i = 0; i < 2; i++)
        {
          SetBuzzerActive(true);
          delay(100);
          SetBuzzerActive(false);
          delay(100);
        }
        SetLatchOpen(true);
        estopDown = false;
      }
      else if (currentMode == MODE_OVERRIDE)
      {
        SetCurrentMode(MODE_NORMAL);
        for (int i = 0; i < 3; i++)
        {
          SetBuzzerActive(true);
          delay(100);
          SetBuzzerActive(false);
          delay(100);
        }
        estopDown = false;
      }
    }
  }
  else
  {
    if (estopDown)
    {
      Serial.println("Estop Up");
      estopDown = false;
    }
  }
}

bool ButtonStateEstop()
{
  temp = mcp.digitalRead(MCP_INPUT_ESTOP);
  return temp ^= IsInvertedESTOP();
}

bool ButtonStateINPUT_UP()
{
  temp = mcp.digitalRead(MCP_INPUT_UP);
  return temp ^= IsInvertedINPUT_UP();
}

bool ButtonStateINPUT_DOWN()
{
  temp = mcp.digitalRead(MCP_INPUT_DOWN);
  return temp ^= IsInvertedINPUT_DOWN();
}

bool ButtonStateINPUT_ALARM()
{
  temp = mcp.digitalRead(MCP_INPUT_ALARM);
  return temp ^= IsInvertedINPUT_ALARM();
}

bool ButtonStateLIFT_MIN()
{
  temp = mcp.digitalRead(MCP_INPUT_LIFT_MIN);
  return temp ^= IsInvertedLIFT_MIN();
}

bool ButtonStateLIFT_MAX()
{
  temp = mcp.digitalRead(MCP_INPUT_LIFT_MAX);
  return temp ^= IsInvertedLIFT_MAX();
}

bool ButtonStateRAMP_MIN()
{
  temp = mcp.digitalRead(MCP_INPUT_RAMP_MIN);
  return temp ^= IsInvertedRAMP_MIN();
}

bool ButtonStateRAMP_MAX()
{
  temp = mcp.digitalRead(MCP_INPUT_RAMP_MAX);
  return temp ^= IsInvertedRAMP_MAX();
}

bool ButtonStateGATE()
{
  temp = mcp.digitalRead(MCP_INPUT_GATE);
  return temp ^= IsInvertedGATE();
}

void EmergencyStop()
{
  SetCurrentMode(MODE_ESTOP_PRESSED);
  Serial.println("EMERGENCY STOP INITIATED");
  ClearTasks(); // forces current task to stop and task buffer to clear;
}

void InputTest()
{
  Serial.print("rampMin: ");
  Serial.print(ButtonStateRAMP_MIN());
  Serial.print("\t");

  Serial.print("rampMax: ");
  Serial.print(ButtonStateRAMP_MAX());
  Serial.print("\t");

  Serial.print("liftMin: ");
  Serial.print(ButtonStateLIFT_MIN());
  Serial.print("\t");

  Serial.print("liftMax: ");
  Serial.print(ButtonStateLIFT_MAX());
  Serial.print("\t");

  Serial.print("gate: ");
  Serial.print(ButtonStateGATE());
  Serial.print("\t");

  Serial.print("up: ");
  Serial.print(ButtonStateINPUT_UP());
  Serial.print("\t");

  Serial.print("down: ");
  Serial.print(ButtonStateINPUT_DOWN());
  Serial.print("\t");

  Serial.print("estop: ");
  Serial.print(ButtonStateEstop());
  Serial.print("\t");

  Serial.print("alarm: ");
  Serial.print(ButtonStateINPUT_ALARM());
  Serial.print("\n");

  Serial.println();

  delay(200);
}

void SetBuzzerActive(bool isOn)
{
  if (isOn)
  {
    mcp.digitalWrite(MCP_PIN_BUZZER, HIGH);
  }
  else
  {
    mcp.digitalWrite(MCP_PIN_BUZZER, LOW);
  }
}

void SetLatchOpen(bool isOn)
{
  isOn ^= IsInvertedLATCH_OUTPUT();
  if (isOn)
  {
    Serial.println("Latch LOW");
    mcp.digitalWrite(MCP_PIN_LATCH, LOW);
  }
  else
  {
    Serial.println("Latch HIGH");
    mcp.digitalWrite(MCP_PIN_LATCH, HIGH);
  }
}

void SetTestMode(bool isOn)
{
  if (isOn)
  {
    SetCurrentMode(MODE_TEST);
  }
  else
  {
    SetCurrentMode(MODE_NORMAL);
  }
}

void SetIndicatorFlashCount(int count)
{
  flashCount = count;
  currentFlashCount = 0;
  digitalWrite(PIN_STATUS_LED, LOW);
}

void FlashIndicator()
{
  if (currentFlashCount < flashCount)
  {
    if (millis() - lastFlash > shortFlashInterval)
    {
      digitalWrite(PIN_STATUS_LED, !digitalRead(PIN_STATUS_LED));
      lastFlash = millis();

      if (!digitalRead(PIN_STATUS_LED))
      {
        currentFlashCount++;
      }
    }
  }
  else
  {
    if (millis() - lastFlash > longFlashInterval)
    {
      digitalWrite(PIN_STATUS_LED, !digitalRead(PIN_STATUS_LED));
      lastFlash = millis();
      currentFlashCount = 0;
    }
  }
}