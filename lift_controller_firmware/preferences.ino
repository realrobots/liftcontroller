// Non-volatile memory interface
// Accesses EEPROM
// Limited by Atmega328 1024 bytes of EEPROM memory

// 0 = invert ESTOP
// 1 = invert INPUT_UP
// 2 = invert MCP_INPUT_DOWN
// 3 = invert MCP_INPUT_ALARM
// 4 = invert LIFT_MIN
// 5 = invert LIFT_MAX
// 6 = invert RAMP_MIN
// 7 = invert RAMP_MAX
// 8 = invert LATCH_OUTPUT
// 9 = invert GATE

// 1023 = isFirstRun 1=NotFirstRun

#include <EEPROM.h>

bool isInverted_ESTOP = 0;
bool isInverted_INPUT_UP = 0;
bool isInverted_INPUT_DOWN = 0;
bool isInverted_INPUT_ALARM = 0;
bool isInverted_LIFT_MIN = 0;
bool isInverted_LIFT_MAX = 0;
bool isInverted_RAMP_MIN = 0;
bool isInverted_RAMP_MAX = 0;
bool isInverted_LATCH_OUTPUT = 0;
bool isInverted_GATE = 0;

String settingString[] = {"INVERT ESTOP", "INVERT INPUT_UP", "INVERT INPUT_DOWN", "INVERT INPUT_ALARM", "INVERT LIFT_MIN", "INVERT LIFT_MAX", "INVERT RAMP_MIN", "INVERT_RAMP_MAX", "INVERT OUTPUT_LATCH", "INVERT INPUT_GATE"};

void LoadSettings()
{    
    if (IsFirstRun())
    {
        RevertDefaults();
    }
    isInverted_ESTOP = Read8BitValue(0);
    isInverted_INPUT_UP = Read8BitValue(1);
    isInverted_INPUT_DOWN = Read8BitValue(2);
    isInverted_INPUT_ALARM = Read8BitValue(3);
    isInverted_LIFT_MIN = Read8BitValue(4);
    isInverted_LIFT_MAX = Read8BitValue(5);
    isInverted_RAMP_MIN = Read8BitValue(6);
    isInverted_RAMP_MAX = Read8BitValue(7);
    isInverted_LATCH_OUTPUT = Read8BitValue(8);
    isInverted_GATE = Read8BitValue(9);
}

void RevertDefaults()
{
    for (int i = 0; i < 10; i++)
    {
        Store8BitValue(i, 0);
    }
}

uint8_t IsInvertedESTOP()
{
    return isInverted_ESTOP;
}
uint8_t IsInvertedINPUT_UP()
{
    return isInverted_INPUT_UP;
}
uint8_t IsInvertedINPUT_DOWN()
{
    return isInverted_INPUT_DOWN;
}
uint8_t IsInvertedINPUT_ALARM()
{
    return isInverted_INPUT_ALARM;
}
uint8_t IsInvertedLIFT_MIN()
{
    return isInverted_LIFT_MIN;
}
uint8_t IsInvertedLIFT_MAX()
{
    return isInverted_LIFT_MAX;
}
uint8_t IsInvertedRAMP_MIN()
{
    return isInverted_RAMP_MIN;
}
uint8_t IsInvertedRAMP_MAX()
{
    return isInverted_RAMP_MAX;
}
uint8_t IsInvertedLATCH_OUTPUT()
{
    return isInverted_LATCH_OUTPUT;
}
uint8_t IsInvertedGATE()
{
    return isInverted_GATE;
}

void PrintSettings()
{
    Serial.println("Current Settings\n");
    for (int i = 0; i < 10; i++)
    {
        Serial.print("$");
        Serial.print(i);
        Serial.print("=");
        Serial.print(Read8BitValue(i));
        Serial.print("\t");
        Serial.println(settingString[i]);
    }
    Serial.println();
}

void ChangeSetting(uint8_t idx, uint8_t val)
{
    Serial.print("Setting ");
    Serial.print(settingString[idx]);
    Serial.print(" to ");
    Serial.println(val);
    if (idx >= 0 && idx < 11 && (val == 0 || val == 1))
    {
        // Serial.print("Setting ");
        // Serial.print(settingString[idx]);
        // Serial.print(" to ");
        // Serial.println(val);
        
        Store8BitValue(idx, val);
        LoadSettings();
        Serial.println("Success");
    }
    else
    {
        Serial.println("Invalid input");
    }
}

bool IsFirstRun()
{
    if (Read8BitValue(1023) != 1)
    {
        Store8BitValue(1023, 1);
        return true;
    }
    else
    {
        return false;
    }
}

void Store8BitValue(uint16_t idx, uint8_t val)
{
    EEPROM.write(idx, val);
}

void Store16BitValue(uint16_t idx, int val)
{
    EEPROM.write(idx, val);
    EEPROM.write(idx + 1, val >> 8);
}

uint8_t Read8BitValue(uint16_t idx)
{
    return EEPROM.read(idx);
}

int Read16BitValue(uint16_t idx)
{
    int val;

    val = (EEPROM.read(idx + 1) << 8);
    val |= EEPROM.read(idx);

    return val;
}
