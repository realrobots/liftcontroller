int accelIncrement = 1; // high number means faster acceleration

void UpdateMotorControl()
{

  if (motor1_target == 0)
  {
    SetMotorSpeed(1, 0);
  }
  else if (motor1_speed < motor1_target)
  {
    if (accelIncrement > 0)
    {
      SetMotorSpeed(1, motor1_speed + accelIncrement);
    }
    else
    {
      SetMotorSpeed(1, motor1_target);
    }
  }
  else if (motor1_speed > motor1_target)
  {
    if (accelIncrement > 0)
    {
      SetMotorSpeed(1, motor1_speed - accelIncrement);
    }
    else
    {
      SetMotorSpeed(1, motor1_target);
    }
  }

  if (motor2_target == 0)
  {
    SetMotorSpeed(2, 0);
  }
  else if (motor2_speed < motor2_target)
  {
    if (accelIncrement > 0)
    {
      SetMotorSpeed(2, motor2_speed + accelIncrement);
    }
    else
    {
      SetMotorSpeed(2, motor2_target);
    }
  }
  else if (motor2_speed > motor2_target)
  {
    if (accelIncrement > 0)
    {
      SetMotorSpeed(2, motor2_speed - accelIncrement);
    }
    else
    {
      SetMotorSpeed(2, motor2_target);
    }
  }
}

void SetMotorSpeed(int motorID, int motorSpeed)
{
  if (motorSpeed > 255)
  {
    motorSpeed = 255;
  }
  else if (motorSpeed < -255)
  {
    motorSpeed = -255;
  }

  if (motorID == 1)
  {
    motor1_speed = motorSpeed;

    if (motorSpeed == 0)
    {
      mcp.digitalWrite(MOTOR1_INH_MCP, LOW);
      digitalWrite(MOTOR1_IN1, LOW);
      digitalWrite(MOTOR1_IN2, LOW);
    }
    else if (motorSpeed > 0)
    {
      mcp.digitalWrite(MOTOR1_INH_MCP, HIGH);
      digitalWrite(MOTOR1_IN1, LOW);
      if (motorSpeed < 100)
      {
        analogWrite(MOTOR1_IN2, 0);
      }
      else
      {
        analogWrite(MOTOR1_IN2, motorSpeed);
      }
    }
    else if (motorSpeed < 0)
    {
      mcp.digitalWrite(MOTOR1_INH_MCP, HIGH);
      digitalWrite(MOTOR1_IN2, LOW);
      if (motorSpeed > -100)
      {
        analogWrite(MOTOR1_IN1, 0);
      }
      else
      {
        analogWrite(MOTOR1_IN1, -motorSpeed);
      }
    }
  }
  else if (motorID == 2)
  {
    motor2_speed = motorSpeed;

    if (motorSpeed == 0)
    {
      mcp.digitalWrite(MOTOR2_INH_MCP, LOW);
      digitalWrite(MOTOR2_IN1, LOW);
      digitalWrite(MOTOR2_IN2, LOW);
    }
    else if (motorSpeed > 0)
    {
      mcp.digitalWrite(MOTOR2_INH_MCP, HIGH);
      digitalWrite(MOTOR2_IN1, LOW);
      if (motorSpeed < 100)
      {
        analogWrite(MOTOR2_IN2, 0);
      }
      else
      {
        analogWrite(MOTOR2_IN2, motorSpeed);
      }
    }
    else if (motorSpeed < 0)
    {
      mcp.digitalWrite(MOTOR2_INH_MCP, HIGH);
      digitalWrite(MOTOR2_IN2, LOW);
      if (motorSpeed > -100)
      {
        analogWrite(MOTOR2_IN1, 0);
      }
      else
      {
        analogWrite(MOTOR2_IN1, -motorSpeed);
      }
    }
  }
}

void Motor2Test()
{
  mcp.digitalWrite(MOTOR2_INH_MCP, HIGH);
  analogWrite(MOTOR2_IN1, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR2_IN2, i);
    Serial.println(i);
    delay(20);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR2_IN2, i);
    Serial.println(i);
    delay(20);
  }

  analogWrite(MOTOR2_IN2, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR2_IN1, i);
    Serial.println(i);
    delay(20);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR2_IN1, i);
    Serial.println(i);
    delay(20);
  }

  delay(1000);
  mcp.digitalWrite(MOTOR2_INH_MCP, LOW);
}

void Motor1Test()
{
  mcp.digitalWrite(MOTOR1_INH_MCP, HIGH);
  analogWrite(MOTOR1_IN1, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR1_IN2, i);
    Serial.println(i);
    delay(20);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR1_IN2, i);
    Serial.println(i);
    delay(20);
  }

  analogWrite(MOTOR1_IN2, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR1_IN1, i);
    Serial.println(i);
    delay(20);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR1_IN1, i);
    Serial.println(i);
    delay(20);
  }

  delay(1000);
  mcp.digitalWrite(MOTOR1_INH_MCP, LOW);
}
